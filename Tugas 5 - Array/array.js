/* Soal No. 1 (Range) 
Buatlah sebuah function dengan nama range() yang menerima dua parameter berupa number. Function mengembalikan sebuah array yang berisi angka-angka mulai dari angka parameter pertama hingga angka pada parameter kedua. Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending).

struktur fungsinya seperti berikut range(startNum, finishNum) {}
Jika parameter pertama dan kedua tidak diisi maka function akan menghasilkan nilai -1

// Code di sini
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 */

function range(startNum, finishNum){
    var cetak = []
    if(startNum < finishNum){
        for(i = startNum; i<=finishNum;i++){
            cetak.push(i)
        }
        return cetak
    }else if(startNum > finishNum){
        for(i = startNum; i>=finishNum;i--){
            cetak.push(i)
        }
        return cetak
    }else{
        return "-1"
    }

    
}

console.log(range(1,10))
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range())
console.log(range(54,50))

/*
Soal No. 2 (Range with Step)
Pada soal kali ini kamu diminta membuat function rangeWithStep yang mirip dengan function range di soal sebelumnya namun parameternya ditambah dengan parameter ketiga yaitu angka step yang menyatakan selisih atau beda dari setiap angka pada array. Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending) dengan step sebesar parameter ketiga.

struktur fungsinya seperti berikut rangeWithStep(startNum, finishNum, step) {}

// Code di sini
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
*/

function rangeWithStep(startNum, finishNum, step) {
    var cetak =[]
    if(startNum < finishNum){
      while(startNum <= finishNum){
        cetak.push(startNum)
        startNum += step
      }
      return cetak
    }else if(startNum > finishNum){
      while(startNum >= finishNum){
        cetak.push(startNum)
        startNum -= step
      }
      return cetak
    }else{
      return "-1"
    }
  }
  
  console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
  console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
  console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
  console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

  /*
  Soal No. 3 (Sum of Range)
  Kali ini kamu akan menjumlahkan sebuah range (Deret) yang diperoleh dari function range di soal-soal sebelumnya. Kamu boleh menggunakan function range dan rangeWithStep pada soal sebelumnya untuk menjalankan soal ini.
  
  Buatlah sebuah function dengan nama sum() yang menerima tiga parameter yaitu angka awal deret, angka akhir deret, dan beda jarak (step). Function akan mengembalikan nilai jumlah (sum) dari deret angka. contohnya sum(1,10,1) akan menghasilkan nilai 55.
  
  ATURAN: Jika parameter ke-3 tidak diisi maka stepnya adalah 1.
  */
  // Code di sini
  function sum(startNum = 0, finishNum = 0, step = 1) {
    var cetak =[]
    if(startNum < finishNum){
      while(startNum <= finishNum){
        cetak.push(startNum)
        startNum += step
      }
    }else{
      while(startNum >= finishNum){
        cetak.push(startNum)
        startNum -= step
      }
    }

    return cetak.reduce((a, b) => a + b, 0)
  }

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

/*
Soal No. 4 (Array Multidimensi)
Sering kali data yang diterima dari database adalah array yang multidimensi (array di dalam array). Sebagai developer, tugas kita adalah mengolah data tersebut agar dapat menampilkan informasi yang diinginkan.

Buatlah sebuah fungsi dengan nama dataHandling dengan sebuah parameter untuk menerima argumen. Argumen yang akan diterima adalah sebuah array yang berisi beberapa array sejumlah n.

*/

function dataHandling (input){
    for(i = 0; i <= input.length - 1 ; i++ ){
      for(j = 0; j <= 4; j++){
        switch(j){
          case 0: {console.log("Nomor ID:" + input[i][j]); break;}
          case 1: {console.log("Nama Lengkap:" + input[i][j]); break;}
          case 2: {console.log("TTL:" + input[i][j] + " " + input[i][j+1]) ; break;}
          case 4: {console.log("Hobi:" + input[i][j]); break;}
        }
      }
      console.log("\n")
    }
}
  
var input = [
                  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
              ] 
  
  
console.log(dataHandling(input))

  /*
  Soal No. 5 (Balik Kata)
Kamu telah mempelajari beberapa method yang dimiliki oleh String dan Array. String sebetulnya adalah sebuah array karena kita dapat mengakses karakter karakter pada sebuah string layaknya mengakses elemen pada array.

Buatlah sebuah function balikKata() yang menerima sebuah parameter berupa string dan mengembalikan kebalikan dari string tersebut.
*/
// Code di sini

function balikKata(str){
    var newString = "";
 
    for (var i = str.length - 1; i >= 0; i--) { 
        newString += str[i]; 
    }

    return newString


}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


function dataHandling2(input){
    var output = input
    var date = []
    
    output.splice(1,2,"Roman Alamsyah Elsharawy")
    output.splice(2,0,"Provinsi Bandar Lampung")
    console.log(output)
    date = output[3].split("/")
    
    switch(parseInt(date[1])){
        case 1: { console.log("Januari"); break; }
        case 2: { console.log("Februari"); break; }
        case 3: { console.log("Maret"); break; }
        case 4: { console.log("April"); break; }
        case 5: { console.log("Mei"); break; }
        case 6: { console.log("Juni"); break; }
        case 7: { console.log("Juli"); break; }
        case 8: { console.log("Agustus"); break; }
        case 9: { console.log("September"); break; }
        case 10: { console.log("Oktober"); break; }
        case 11: { console.log("November"); break; }
        case 12: { console.log("Desember"); break; }
        default:  { console.log('Format bulan salah'); }
    
    }
    console.log(date.sort(function (value1, value2) { return value1 < value2 }))
    var cetak = date.join("-")
    console.log(cetak)

    cetak = output[1]
    console.log(cetak.slice(0,14))


}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 