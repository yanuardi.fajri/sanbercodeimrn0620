function arrayToObject(arr) {
    // Code di sini 
    var cetak = {}
    var now = new Date()
    var name = ""
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    for(i=0;i<=arr.length-1;i++){
        for(j=0;j<=3;j++){
            switch(j){
                case 0:{ cetak.firstname = arr[i][j]; name = arr[i][j] + " "; break;}
                case 1:{ cetak.lastname = arr[i][j]; name += arr[i][j]; break;}
                case 2:{ cetak.gender = arr[i][j]; break;}
                case 3:{ 
                    if(arr[i][j]<=thisYear){
                        cetak.age = thisYear - arr[i][j]; 
                    }else{
                        cetak.age = "Invalid Birth Year"
                    }
                    break;
                }
            }
        }
        console.log((i+1) + ". " + name + ":")
        console.log(cetak)
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

function shoppingTime(memberId, money) {
    // you can only write your code here!
    
    var cart = {}
    var harga = [1500000,500000,250000,175000,50000]
    var barang = ["Sepatu Stacattu","Baju Zoro","Baju H&N","Sweater Uniklooh","Casing Handphone"]
    var buyBarang = []
    if(memberId){
        if(money && money>50000){
            cart.memberId = memberId
            cart.money = money
            for(i=0;i<harga.length;i++){
                if(money>=harga[i]){
                    money -= harga[i]
                    buyBarang.push(barang[i])
                }
            }
            cart.listPurchased = buyBarang
            cart.changeMoney = money
            return cart

        }else{
            return "Mohon maaf, uang tidak cukup"
        }

    }else{
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    let total = 0
    
    let cetak = []
    
    for(i=0;i<arrPenumpang.length;i++){
        let penumpang = {}
        penumpang.penumpang = arrPenumpang[i][0]
        penumpang.naikDari = arrPenumpang[i][1]
        penumpang.tujuan = arrPenumpang[i][2]
        total = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000
        penumpang.bayar = total
        
        cetak.push(penumpang)
    }

    return cetak
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]